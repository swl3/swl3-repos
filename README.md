# Репозитории SWL3 
Просто проект, в пакетах которого хранятся все модули swl3

## Подключение

  - ### Gradle:

  _build.gradle_:
  ```gradle
  repositories {
    maven { url "https://gitlab.com/api/v4/projects/21245256/packages/maven"}
  }
  ```

  - ### Maven:

  _pom.xml_:
  ```xml
  <repositories>
    <repository>
      <id>gitlab-maven</id>
      <url>https://gitlab.com/api/v4/projects/21245256/packages/maven</url>
    </repository>
  </repositories>

  <distributionManagement>
    <repository>
      <id>gitlab-maven</id>
      <url>https://gitlab.com/api/v4/projects/21245256/packages/maven</url>
    </repository>

    <snapshotRepository>
      <id>gitlab-maven</id>
      <url>https://gitlab.com/api/v4/projects/21245256/packages/maven</url>
    </snapshotRepository>
  </distributionManagement>
  ```
## Пакеты:
https://gitlab.com/swl3/swl3-repos/-/packages
